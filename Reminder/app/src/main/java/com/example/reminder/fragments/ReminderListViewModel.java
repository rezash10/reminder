package com.example.reminder.fragments;

import static com.example.reminder.Utils.G_TAG;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.reminder.model.ReminderEntity;
import com.example.reminder.model.ReminderRoomDB;

import java.util.ArrayList;
import java.util.List;

public class ReminderListViewModel extends AndroidViewModel {
    //---------variables----------//
    Application application;
    private MutableLiveData<List<ReminderEntity>> reminders = new MutableLiveData<>();
    public MutableLiveData<Boolean> hasItems = new MutableLiveData<>();

    public LiveData<List<ReminderEntity>> getReminders(){
        return reminders;
    }
    public ReminderListViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        getRemindersFromDb();
    }

    private void getRemindersFromDb() {

        new Thread(new Runnable() {
            @Override
            public void run() {


                ReminderRoomDB db = ReminderRoomDB.getInstance(application);
                List<ReminderEntity> remindersList = db.reminderDao().getReminders();


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(!remindersList.isEmpty()){
                            hasItems.setValue(true);
                        }else{
                            hasItems.setValue(false);
                        }
                        reminders.setValue(remindersList);
                    }
                });

            }
        }).start();
    }


}
