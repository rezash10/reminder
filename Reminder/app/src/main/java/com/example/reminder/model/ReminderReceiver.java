package com.example.reminder.model;

import static com.example.reminder.Utils.CHANNEL_ID;
import static com.example.reminder.Utils.G_TAG;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.reminder.MainActivity;
import com.example.reminder.R;

public class ReminderReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent!=null){




            String title = intent.getExtras().getString("title");
            String body = intent.getExtras().getString("body");
            long key = intent.getExtras().getLong("key");

            Intent intent1 = new Intent(context, MainActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context,2,intent1,
                    PendingIntent.FLAG_UPDATE_CURRENT);


            Notification notification = new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.offclock)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .build();


            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            manager.notify(1,notification);

            dismissReminder(key,context);
        }

    }

    private void dismissReminder(long key,Context context) {

       new Thread(new Runnable() {
           @Override
           public void run() {

               ReminderRoomDB db = ReminderRoomDB.getInstance(context);

               db.reminderDao().updateReminderStatus(key,false);
           }
       }).start();

    }
}
