package com.example.reminder.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = ReminderEntity.class,exportSchema = false,version = 1)
abstract public class ReminderRoomDB extends RoomDatabase {

    static ReminderRoomDB instance;

    public static synchronized ReminderRoomDB getInstance(Context context){
        if(instance==null){
            //-------create roomDb---------//
           instance =  Room.databaseBuilder(context,ReminderRoomDB.class,"ReminderDB")
                    .fallbackToDestructiveMigration().build();
        }

        return instance;

    }

    public abstract ReminderDao reminderDao();

}
