package com.example.reminder;

import static com.example.reminder.Utils.G_TAG;
import static com.example.reminder.fragments.RemindersListFragment.reminderFrag;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reminder.databinding.ModelReminderItemBinding;
import com.example.reminder.fragments.RemindersListFragmentDirections;
import com.example.reminder.model.ReminderEntity;
import com.example.reminder.model.ReminderReceiver;
import com.example.reminder.model.ReminderRoomDB;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.transition.MaterialSharedAxis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RemindersAdapter extends ListAdapter<ReminderEntity, RemindersAdapter.ViewHolder> {

    Context ctx;
    public RemindersAdapter(Context context) {
        super(new ReminderDiffUtil());
        ctx = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ModelReminderItemBinding binding = ModelReminderItemBinding.inflate(inflater
                ,parent,false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.setupItems(getItem(position));


        holder.binding.ivDeleteAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new MaterialAlertDialogBuilder(ctx,R.style.alertdialog).setTitle("ایا مایل به حذف یادآور هستید؟")
                        .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteAlarmManager(holder.getAdapterPosition());
                            }
                        }).setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setCancelable(true).show();

            }
        });

        holder.binding.ivEditAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                NavController controller = Navigation.findNavController(view);

                RemindersListFragmentDirections.ActionRemindersListFragmentToProductReminderFragment
                        action  = RemindersListFragmentDirections.actionRemindersListFragmentToProductReminderFragment();

                action.setTitle(getCurrentList().get(holder.getAdapterPosition()).getTitle());
                action.setBody(getCurrentList().get(holder.getAdapterPosition()).getBody());
                action.setKey(getCurrentList().get(holder.getAdapterPosition()).getKey());

                reminderFrag.setExitTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,true)
                        .setDuration(1000));

                reminderFrag.setReenterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,false)
                        .setDuration(1000));


                controller.navigate(action);


            }
        });
    }

    private void deleteAlarmManager(int position) {


        AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(ctx, ReminderReceiver.class);
        intent.putExtra("delete","delete");

        PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,(int)getCurrentList().get(position)
                .getKey(),intent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);

        ArrayList<ReminderEntity> arr = new ArrayList<>(getCurrentList());
        arr.remove(position);
        submitList(arr);

        new Thread(new Runnable() {
            @Override
            public void run() {
                ReminderRoomDB db = ReminderRoomDB.getInstance(ctx.getApplicationContext());
                db.reminderDao().deleteReminder(getCurrentList().get(position).getKey());

            }
        }).start();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ModelReminderItemBinding binding;
        public ViewHolder(@NonNull ModelReminderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setupItems(ReminderEntity entity){
            binding.setReminderEntity(entity);
            binding.executePendingBindings();

        }
    }
}
