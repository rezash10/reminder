package com.example.reminder.fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.reminder.R;
import com.example.reminder.databinding.FragmentRemindersListBinding;
import com.google.android.material.transition.MaterialSharedAxis;



public class RemindersListFragment extends Fragment {

    public static Fragment reminderFrag;
    private static final String TAG = "RemindersListFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentRemindersListBinding binding = FragmentRemindersListBinding.inflate(inflater);

        reminderFrag = requireActivity().getSupportFragmentManager().findFragmentById(R.id.main_nav)
                .getChildFragmentManager().getFragments().get(0);



        ReminderListViewModel viewModel = new ViewModelProvider(this).get(ReminderListViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP){
            ViewGroupCompat.setTransitionGroup(binding.clReminderList, true);
        }

        viewModel.hasItems.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){

                        binding.rvMain.setVisibility(View.VISIBLE);
                        binding.pbLoading.setVisibility(View.GONE);



                }else{
                    binding.pbLoading.setVisibility(View.GONE);
                    binding.txNoReminder.setVisibility(View.VISIBLE);
                    binding.ivNoReminder.setVisibility(View.VISIBLE);
                }
            }
        });



        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return binding.getRoot();
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.ic_add_reminder){

            reminderFrag.setExitTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,true)
            .setDuration(1000));

            reminderFrag.setReenterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,false)
            .setDuration(1000));



            NavController navController = Navigation.findNavController(requireActivity(),R.id.main_nav);
            navController.navigate(R.id.action_remindersListFragment_to_productReminderFragment);
        }
        return super.onOptionsItemSelected(item);
    }
}