package com.example.reminder.fragments;

import static com.example.reminder.Utils.G_TAG;
import static com.example.reminder.Utils.prepareDay;
import static com.example.reminder.Utils.prepareHour;
import static com.example.reminder.Utils.prepareMinute;
import static com.example.reminder.Utils.prepareMonth;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.reminder.R;
import com.example.reminder.databinding.FragmentProductReminderBinding;
import com.example.reminder.model.ReminderEntity;
import com.example.reminder.model.ReminderRoomDB;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.transition.MaterialSharedAxis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ProductReminderFragment extends Fragment {

    private static final String TAG = "ProductReminderFragment";
    private int currentYear = -1;
    private int currentMonth = -1;
    private int currentDay = -1;
    private int currentHour = -1;
    private int currentMinute = -1;

    ArrayAdapter<Integer> minuteAdapter = null;
    ArrayAdapter<Integer> hourAdapter = null;
    ArrayAdapter<Integer> dayAdapter = null;
    ArrayAdapter<Integer> monthAdapter = null;
    ArrayAdapter<Integer> yearAdapter = null;

    int selectedMinute = -1;
    int selectedHour = -1;
    int selectedDay = -1;
    int selectedMonth = -1;
    int selectedYear = -1;



    Spinner yearSpinner;
    Spinner monthSpinner;
    Spinner daySpinner;
    Spinner hourSpinner;
    Spinner minuteSpinner;




    ProductReminderViewModel viewModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //-------------setup sharedMaterial transaction---------------//
        Fragment productRemindersFrag = requireActivity().getSupportFragmentManager().findFragmentById(R.id.main_nav)
                .getChildFragmentManager().getFragments().get(0);

        Log.i(TAG, productRemindersFrag.toString());
        Log.i(TAG, getClass().getName());

        productRemindersFrag.setEnterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z, true)
                .setDuration(1000));
        productRemindersFrag.setReturnTransition(new MaterialSharedAxis(MaterialSharedAxis.Z, false)
                .setDuration(1000));


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentProductReminderBinding binding = FragmentProductReminderBinding.inflate(inflater);

        viewModel = new ViewModelProvider(this).get(ProductReminderViewModel.class);


        if (!viewModel.title.isEmpty()) {
            binding.etReminderTitle.getEditText().setText(viewModel.title);
        }

        if (!viewModel.body.isEmpty()) {
            binding.etReminderBody.getEditText().setText(viewModel.body);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ViewGroupCompat.setTransitionGroup(binding.clProductReminder, true);
        }


        yearSpinner = binding.spYear;
        monthSpinner = binding.spMonth;
        daySpinner = binding.spDay;
        hourSpinner = binding.spHour;
        minuteSpinner = binding.spMinute;



        String title = ProductReminderFragmentArgs.fromBundle(getArguments()).getTitle();
        String body = ProductReminderFragmentArgs.fromBundle(getArguments()).getBody();

        long reminderKey = ProductReminderFragmentArgs.fromBundle(getArguments()).getKey();

        if (!title.isEmpty()) {
            binding.etReminderTitle.getEditText().setText(title);
            binding.etReminderBody.getEditText().setText(body);
        }


        //-------------handling years changes----------------//

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedYear = (int) adapterView.getItemAtPosition(i);


                List<Integer> hourList = null;
                List<Integer> minuteList = null;
                List<Integer> dayList = null;
                List<Integer> monthList = null;


                if (selectedYear == currentYear) {


                    hourList = prepareHour(currentHour);
                    minuteList = prepareMinute(currentMinute);
                    dayList = prepareDay(currentDay, currentMonth);
                    monthList = prepareMonth(currentMonth);

                    selectedMonth = currentMonth;
                    selectedDay = currentDay;
                    selectedHour = currentHour;
                    selectedMinute = currentMinute;


                } else {



                    hourList = prepareHour(0);
                    minuteList = prepareMinute(60);
                    dayList = prepareDay(1, 1);
                    monthList = prepareMonth(1);

                    selectedMonth = 1;
                    selectedDay = 1;
                    selectedHour = 0;
                    selectedMinute = 0;


                }


                monthAdapter.clear();
                monthAdapter.addAll(monthList);
                dayAdapter.clear();
                dayAdapter.addAll(dayList);
                hourAdapter.clear();
                hourAdapter.addAll(hourList);
                minuteAdapter.clear();
                minuteAdapter.addAll(minuteList);

                monthSpinner.setSelection(0);
                daySpinner.setSelection(0);
                hourSpinner.setSelection(0);
                minuteSpinner.setSelection(0);



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //-------------handling months changes----------------//

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {



                selectedMonth = (int) adapterView.getItemAtPosition(i);




                List<Integer> hourList = null;
                List<Integer> minuteList = null;
                List<Integer> dayList = null;


                if (selectedMonth == currentMonth && selectedYear == currentYear) {


                    hourList = prepareHour(currentHour);
                    minuteList = prepareMinute(currentMinute);
                    dayList = prepareDay(currentDay, currentMonth);

                    selectedDay = currentDay;
                    selectedHour = currentHour;
                    selectedMinute = currentMinute;


                } else {

                    hourList = prepareHour(0);
                    minuteList = prepareMinute(60);
                    dayList = prepareDay(1, selectedMonth);

                    selectedDay = 1;
                    selectedHour = 0;
                    selectedMinute = 0;


                }



                dayAdapter.clear();
                dayAdapter.addAll(dayList);
                hourAdapter.clear();
                hourAdapter.addAll(hourList);
                minuteAdapter.clear();
                minuteAdapter.addAll(minuteList);


                daySpinner.setSelection(0);
                hourSpinner.setSelection(0);
                minuteSpinner.setSelection(0);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //-------------handling days changes----------------//

        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                selectedDay = (int) adapterView.getItemAtPosition(i);





                List<Integer> hourList = null;
                List<Integer> minuteList = null;
                if (selectedDay == currentDay && selectedYear == currentYear && selectedMonth == currentMonth) {


                    hourList = prepareHour(currentHour);
                    minuteList = prepareMinute(currentMinute);

                    selectedHour = currentHour;
                    selectedMinute = currentMinute;


                } else {

                    hourList = prepareHour(0);
                    minuteList = prepareMinute(60);

                    selectedHour = 0;
                    selectedMinute = 0;


                }

                hourAdapter.clear();
                hourAdapter.addAll(hourList);
                minuteAdapter.clear();
                minuteAdapter.addAll(minuteList);

                hourSpinner.setSelection(0);
                minuteSpinner.setSelection(0);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //-------------handling hours changes----------------//

        hourSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedHour = (int) adapterView.getItemAtPosition(i);




                List<Integer> minuteList = null;
                if (selectedHour == currentHour && selectedYear == currentYear && selectedMonth == currentMonth && selectedDay == currentDay) {

                    minuteList = prepareMinute(currentMinute);
                    selectedMinute = currentMinute;

                } else {



                    minuteList = prepareMinute(60);

                    selectedMinute = 0;


                }

                minuteAdapter.clear();
                minuteAdapter.addAll(minuteList);


                minuteSpinner.setSelection(0);





            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //-------------handling minutes changes----------------//

        minuteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                selectedMinute = (int) adapterView.getItemAtPosition(i);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //----------observer year-------------//

        viewModel.yearLiveData.observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {

                currentYear = integers.get(0);


                yearAdapter = new ArrayAdapter<Integer>(requireContext(),
                        R.layout.spinner_item, integers);

                yearSpinner.setAdapter(yearAdapter);


            }
        });

        //----------observer month-------------//

        viewModel.monthLiveData.observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {

                currentMonth = integers.get(0);


                monthAdapter = new ArrayAdapter<Integer>(requireContext(),
                        R.layout.spinner_item, integers);

                monthSpinner.setAdapter(monthAdapter);


            }
        });

        //----------observer day-------------//

        viewModel.dayLiveData.observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {

                currentDay = integers.get(0);

                dayAdapter = new ArrayAdapter<Integer>(requireContext(),
                        R.layout.spinner_item, integers);

                daySpinner.setAdapter(dayAdapter);


            }
        });

        //----------observer hour-------------//

        viewModel.hourLiveData.observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {

                currentHour = integers.get(0);


                hourAdapter = new ArrayAdapter<Integer>(requireContext(),
                        R.layout.spinner_item, integers);

                hourSpinner.setAdapter(hourAdapter);


            }
        });

        //----------observer minute-------------//

        viewModel.minuteLiveData.observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {


                currentMinute = integers.get(0);



                minuteAdapter = new ArrayAdapter<Integer>(requireContext(),
                        R.layout.spinner_item, integers);

                minuteSpinner.setAdapter(minuteAdapter);


            }
        });


        Button save = binding.btnSave;

        //-----------handling error icon-------------//
        binding.etReminderTitle.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (binding.etReminderTitle.getError() != null)
                    binding.etReminderTitle.setError(null);
                viewModel.title = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etReminderBody.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                viewModel.body = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String reminderTitle = binding.etReminderTitle.getEditText().getText().toString().trim();
                String reminderBody = binding.etReminderBody.getEditText().getText().toString().trim();

                if (reminderTitle.isEmpty()) {
                    binding.etReminderTitle.setError("عنوان را وارد کنید");
                    return;
                }


                //-----------change Shamsi time to Georgian time-----------//

                int leapYear = ((selectedYear - 1) / 4);
                int currentYearDays = getCurrentYearDays(selectedMonth, selectedDay);
                int totalDays = ((selectedYear - 1) * 365) + leapYear + currentYearDays;

                int total = totalDays + 226899 - (leapYear + 155);

                int geoYear = total / 365 + 1;
                int daysLeft = total % (total / 365);

                Log.i(TAG, "year: " + geoYear);
                Log.i(TAG, "daysLeft: " + daysLeft);

                int[] geoMonthAndDay = getMonth(daysLeft);
                int geoMonth = geoMonthAndDay[0];
                int geoDay = geoMonthAndDay[1];

                Log.i(TAG, "month: " + geoMonth);
                Log.i(TAG, "day: " + geoDay);

                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                Date date = new Date();
                String time = format.format(date);

                String[] timeArr = time.split(" ");
                String ymd = timeArr[0];
                String hm = timeArr[1];

                String[] ymdArr = ymd.split("/");
                String[] hmArr = hm.split(":");

                int currentY = Integer.parseInt(ymdArr[0]);
                int currentM = Integer.parseInt(ymdArr[1]);
                int currentD = Integer.parseInt(ymdArr[2]);

                int currentMin = Integer.parseInt(hmArr[1]);
                int currentH = Integer.parseInt(hmArr[0]);


                if (geoYear <= currentY && geoMonth <= currentM &&
                        geoDay <= currentD && selectedHour <= currentH && selectedMinute <= currentMin) {
                    Snackbar.make(binding.getRoot(), "تنظیم یادآور برای این زمان ممکن نیست", Snackbar.LENGTH_LONG).show();
                    return;
                }


                long key = System.currentTimeMillis();

                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        ReminderEntity entity = new ReminderEntity(selectedYear, selectedMonth
                                , selectedDay, selectedHour, selectedMinute, reminderTitle, reminderBody
                                , key, true);

                        ReminderRoomDB roomDB = ReminderRoomDB.getInstance(requireContext());

                        if (!title.isEmpty()) {
                            //update db
                            roomDB.reminderDao().updateReminder(selectedYear, selectedMonth,
                                    selectedDay, selectedHour, selectedMinute, reminderTitle, reminderBody
                                    , reminderKey,true);

                        } else {
                            roomDB.reminderDao().insertReminder(entity);

                        }

                    }
                }).start();


                //-----------setup alarm manager -------------//

                if (!title.isEmpty()) {
                    viewModel.setReminderAlertManager(geoYear, geoMonth, geoDay, selectedHour, selectedMinute
                            , reminderTitle, reminderBody, reminderKey);
                    Toast.makeText(requireActivity(), "یادآور شما بروز شد", Toast.LENGTH_SHORT).show();

                } else {
                    viewModel.setReminderAlertManager(geoYear, geoMonth, geoDay, selectedHour, selectedMinute
                            , reminderTitle, reminderBody, key);
                    Toast.makeText(requireActivity(), "یادآور شما ایجاد شد", Toast.LENGTH_SHORT).show();
                }


                NavController controller = Navigation.findNavController(requireActivity(), R.id.main_nav);
                controller.navigate(R.id.action_productReminderFragment_to_remindersListFragment);
            }
        });

        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController controller = Navigation.findNavController(requireActivity(), R.id.main_nav);
                controller.navigate(R.id.action_productReminderFragment_to_remindersListFragment);
            }
        });


//         Inflate the layout for this fragment

        return binding.getRoot();
    }




    private int[] getMonth(int daysLeft) {

        int[] arr = new int[2];
        if (daysLeft <= 181) {
            //first part of the year
            if (daysLeft <= 90) {
                //first three months
                if (daysLeft <= 59) {
                    //first two month
                    if (daysLeft <= 31) {
                        arr[0] = 1;
                        arr[1] = daysLeft;
                        return arr;

                    } else {
                        arr[0] = 2;
                        arr[1] = daysLeft - 31;
                        return arr;
                    }

                } else {

                    arr[0] = 3;
                    arr[1] = daysLeft - 59;
                    return arr;


                }
            } else {

                if (daysLeft <= 151) {
                    //first two month
                    if (daysLeft <= 120) {
                        arr[0] = 4;
                        arr[1] = daysLeft - 90;
                        return arr;

                    } else {
                        arr[0] = 5;
                        arr[1] = daysLeft - 120;
                        return arr;
                    }

                } else {

                    arr[0] = 6;
                    arr[1] = daysLeft - 151;
                    return arr;

                }

            }

        } else {

            if (daysLeft <= 273) {
                //first three months
                if (daysLeft <= 243) {
                    //first two month
                    if (daysLeft <= 212) {
                        arr[0] = 7;
                        arr[1] = daysLeft - 181;
                        return arr;

                    } else {
                        arr[0] = 8;
                        arr[1] = daysLeft - 212;
                        return arr;
                    }

                } else {

                    arr[0] = 9;
                    arr[1] = daysLeft - 243;
                    return arr;

                }
            } else {

                if (daysLeft <= 334) {
                    //first two month
                    if (daysLeft <= 304) {
                        arr[0] = 10;
                        arr[1] = daysLeft - 273;
                        return arr;

                    } else {
                        arr[0] = 11;
                        arr[1] = daysLeft - 304;
                        return arr;
                    }

                } else {

                    arr[0] = 12;
                    arr[1] = daysLeft - 334;
                    return arr;

                }

            }

        }
    }

    private int getCurrentYearDays(int month, int day) {
        int monthDayes = 0;
        switch (month - 1) {
            case 1: {
                monthDayes = 31;
                break;
            }
            case 2: {
                monthDayes = 62;
                break;
            }
            case 3: {
                monthDayes = 93;
                break;
            }
            case 4: {
                monthDayes = 124;
                break;
            }
            case 5: {
                monthDayes = 155;
                break;
            }
            case 6: {
                monthDayes = 186;
                break;
            }
            case 7: {
                monthDayes = 216;
                break;
            }
            case 8: {
                monthDayes = 246;
                break;
            }
            case 9: {
                monthDayes = 276;
                break;
            }
            case 10: {
                monthDayes = 306;
                break;
            }
            case 11: {
                monthDayes = 336;
                break;
            }
            case 12: {
                monthDayes = 365;
                break;
            }
        }

        monthDayes += day;

        return monthDayes;
    }
}