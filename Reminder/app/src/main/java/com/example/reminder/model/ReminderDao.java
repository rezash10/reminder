package com.example.reminder.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ReminderDao {
    @Insert
    void insertReminder(ReminderEntity entity);

    @Query("SELECT * FROM ReminderDB ORDER BY `id` DESC")
    List<ReminderEntity> getReminders();

    @Query("Update ReminderDB SET `isEnable`=:status WHERE `key` LIKE :key")
    void updateReminderStatus(long key, boolean status);

    @Query("Update ReminderDB SET `year`=:year , `month`=:month , `day`=:day , `hour`=:hour" +
            ", `minute`=:minute , `title`=:title , `body`=:body , `isEnable`=:status WHERE `key` LIKE :key")
    void updateReminder(int year,int month,int day,int hour,int minute,String title,String body
    ,long key,boolean status);

    @Query("DELETE FROM ReminderDB WHERE `key` LIKE :key")
    void deleteReminder(long key);


}
