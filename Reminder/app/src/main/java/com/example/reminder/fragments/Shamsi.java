package com.example.reminder.fragments;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Shamsi {

    private static final String TAG = "Shamsi_tag";

    private int shamsiYear;
    private int shamsiMonth;
    private int shamsiDay;
    private int shamsiHour;
    private int shamsiMinute;

    public Shamsi() {
        convertGregorianToShamsi();
    }

    private void convertGregorianToShamsi() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date date = new Date();
        String dateStr = timeFormat.format(date);


        String[] timeDivider = dateStr.split(" ");
        String dailyTime = timeDivider[0];
        String hourTime = timeDivider[1];



        String[] dailyArr = dailyTime.split("/");
        String[] hourArr = hourTime.split(":");


        shamsiHour = Integer.parseInt(hourArr[0]);
        shamsiMinute = Integer.parseInt(hourArr[1]);

        int year = Integer.parseInt(dailyArr[0]);
        int month = Integer.parseInt(dailyArr[1]);
        int day = Integer.parseInt(dailyArr[2]);
        int hour = Integer.parseInt(hourArr[0]);
        int minute = Integer.parseInt(hourArr[1]);

        int currentYearDays = getCurrentYearDays(month,day);
        int leapYears = (year-1)/4;
        int totalDays = ((year-1)*365)+leapYears+currentYearDays;
        int differentDays = 226899;


        int shamsiLeapYear = leapYears-155;
        int shamsiDays = totalDays-differentDays-shamsiLeapYear;

        int resYear = shamsiDays/365+1;
        int daysLeft = shamsiDays%(shamsiDays/365);



        int[] getMonthAndDay = convertDaysToMonthAndDays(daysLeft);


        int resMonth = getMonthAndDay[0];
        int resDay = getMonthAndDay[1];

        shamsiYear = resYear;
        shamsiMonth = resMonth;
        shamsiDay = resDay;




    }

    private int[] convertDaysToMonthAndDays(int daysLeft) {

        int[] monthAndDay = new int[2];
        //----------first half of the year----------//
        if(daysLeft<=186){
            int months = daysLeft/31;
            int days = daysLeft-(months*31);

            if(days==0){
                days=31;
            }else{
                months+=1;
            }



            monthAndDay[0] = months;
            monthAndDay[1] = days;
        }else{
            daysLeft-=186;

            int months = (daysLeft/30);
            int days = daysLeft-(months*30);

            months+=6;
            if(days==0){
                days = 30;
            }else{
                months+=1;
            }

            monthAndDay[0] = months;
            monthAndDay[1] = days;

        }
        return monthAndDay;
    }

    private int getCurrentYearDays(int month, int day) {
        int monthDayes = 0;
        switch (month-1){
            case 1: {
                monthDayes = 31;
                break;
            }
            case 2:{
                monthDayes = 59;
                break;
            }
            case 3:{
                monthDayes = 90;
                break;
            }
            case 4:{
                monthDayes = 120;
                break;
            }
            case 5:{
                monthDayes = 151;
                break;
            }
            case 6:{
                monthDayes = 181;
                break;
            }
            case 7:{
                monthDayes = 212;
                break;
            }
            case 8:{
                monthDayes = 243;
                break;
            }
            case 9:{
                monthDayes = 273;
                break;
            }
            case 10:{
                monthDayes = 304;
                break;
            }
            case 11:{
                monthDayes = 334;
                break;
            }
            case 12:{
                monthDayes = 365;
                break;
            }
        }
        monthDayes+=day;
        return monthDayes;
    }

    public int getShamsiYear() {
        return shamsiYear;
    }

    public int getShamsiMonth() {
        return shamsiMonth;
    }

    public int getShamsiDay() {
        return shamsiDay;
    }

    public int getShamsiHour() {
        return shamsiHour;
    }

    public int getShamsiMinute() {
        return shamsiMinute;
    }
}
